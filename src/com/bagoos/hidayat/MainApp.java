package com.bagoos.hidayat;

import java.util.*;

public class MainApp {

    public static final String DELIMITER_SPACE = " ";
    public static final String IS_STRING = "is";
    public static final String CREDITS_STRING = "credits";

    public static final String PRINT_CURRENCY_COMMAND = "print currency";
    public static final String PRINT_ITEM_COMMAND = "print item";
    public static final String EXIT_COMMAND = "exit";
    public static final String HELP_COMMAND = "help";

    public static final String ONE_IN_ROMAN = "I";
    public static final String FIVE_IN_ROMAN = "V";
    public static final String TEN_IN_ROMAN = "X";
    public static final String FIFTY_IN_ROMAN = "L";
    public static final String ONE_HUNDRED_IN_ROMAN = "C";
    public static final String FIVE_HUNDRED_IN_ROMAN = "D";
    public static final String ONE_THOUSAND_IN_ROMAN = "M";

    public static final List<String> LIST_ROMAN_NUMERIC_SYMBOL = Arrays.asList(
            ONE_IN_ROMAN,
            FIVE_IN_ROMAN,
            TEN_IN_ROMAN,
            FIFTY_IN_ROMAN,
            ONE_HUNDRED_IN_ROMAN,
            FIVE_HUNDRED_IN_ROMAN,
            ONE_THOUSAND_IN_ROMAN
    );

    public static final String REGEX_ROMAN_NUMERIC = "M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})";

    public static void main(String[] args) throws Exception {
        try {
            String keepgoing = DELIMITER_SPACE;

            Scanner scanner = new Scanner(System.in);
            System.out.println("input command...");

            Map<String, String> mapRomanToCurrency = new HashMap<String, String>();
            Map<String, String> mapCurrencyToRoman = new HashMap<String, String>();
            Map<String, Integer> mapItemToValue = new HashMap<String, Integer>();

            while (keepgoing.equalsIgnoreCase(DELIMITER_SPACE)) {

                String command = scanner.nextLine();
                // System.out.println("command detected = " + command);

                confirmCommand(command, mapRomanToCurrency, mapCurrencyToRoman, mapItemToValue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void confirmCommand(String command, Map<String, String> mapRomanToCurrency, Map<String, String> mapCurrencyToRoman, Map<String, Integer> mapItemToValue) {
        boolean valid = true;

        // validation of basic commands
        if (command.equalsIgnoreCase(PRINT_CURRENCY_COMMAND))
            printAllRegisteredCurrencyCode(mapRomanToCurrency, mapCurrencyToRoman);

        if (command.equalsIgnoreCase(EXIT_COMMAND))
             System.exit(1);

        if (command.equalsIgnoreCase(HELP_COMMAND)) {
             System.out.println("write \"{currencyCode} is {romanNumeric}\" to register currency code into roman numeric...");
             System.out.println("write \"{registered currencyCode currencyCode ...} {item} is {numeric} Credits\" to convert item value from credits into currency code...");
             System.out.println("write PRINT CURRENCY to see all registered currency code...");
             System.out.println("write PRINT ITEM to see all items value in currency code...");
             System.out.println("write EXIT to exit the application...");
        }

        // split command into array of word
        String[] commandArray = command.split(DELIMITER_SPACE);

        // code blocks for registering currency code
        // detected : command for registering currency code
        // valid format : {currencyCode} is {romanNumeric}
        // main condition : array length is 3, and second word equals to "is"
        // valid condition :
        // - third word is valid roman numeric
        // - currency code (first word) is not assigned into any roman numeric
        // - roman numeric is not assigned by any currency code

        if (commandArray.length == 3) {
            // System.out.println("consists of 3 words");
            String getCurrencyCode = commandArray[0];
            String getSecondWord = commandArray[1];

            if (getSecondWord.equalsIgnoreCase(IS_STRING)) {
                // System.out.println("second word equals to is");
                String getRomanNumeric = commandArray[2];

                // command for storing : handling invalid request
                // 1. roman numerals are invalid characters (valid : I, V, X, L, C, D, and M)
                if (!LIST_ROMAN_NUMERIC_SYMBOL.contains(getRomanNumeric)) {
                    System.out.println("[INVALID] " + getRomanNumeric + " is not roman numerals.");
                    valid = false;
                }
                else
                    System.out.println("[VALID] " + getRomanNumeric + " is roman numerals.");

                // 2. roman numerals has been used in another currency code
                String getCurrencyFromMap = mapRomanToCurrency.get(getRomanNumeric);
                // System.out.println("getCurrencyFromMap = " + getCurrencyFromMap);

                if (getCurrencyFromMap != null) {
                    System.out.println("[INVALID] " + getRomanNumeric + " has been assigned by " + getCurrencyFromMap);
                    valid = false;
                } else
                    System.out.println("[VALID] " + getRomanNumeric + " is not assigned by any currency code.");

                // 3. currency code has been used in another roman numerals
                String getRomanNumericFromMap = mapCurrencyToRoman.get(getCurrencyCode);
                // System.out.println("getRomanNumericFromMap = " + getRomanNumericFromMap);

                if (getRomanNumericFromMap != null) {
                    System.out.println("[INVALID] " + getCurrencyCode + " has been assigned to " + getRomanNumericFromMap);
                    valid = false;
                } else
                    System.out.println("[VALID] " + getCurrencyCode + " is not assigned to any roman numeric.");

                if (valid) {
                    mapRomanToCurrency.put(getRomanNumeric, getCurrencyCode);
                    mapCurrencyToRoman.put(getCurrencyCode, getRomanNumeric);
                    System.out.println("[SUCCESS] " + getCurrencyCode + " has been assigned by " + getRomanNumeric);
                }
            }
        }

        // code blocks for registering currency code - END

        // --------------------------------------------------------------------------------

        // code blocks for convert item value from credits into currency code - START
        // detected : command for convert item value from credits into currency code
        // valid format : {registered currencyCode currencyCode ...} {itemName} is {integerValue} Credits
        // main condition : last word of command is "Credits"
        // valid condition :
        // - second last word of command is Integer value (value of credits)
        // - third last word of command is "is" word
        // - fourth last word of command is item name
        // - currency code is registered
        // - currency code is written in valid roman numeric rule
        // -- valid roman numeric rule :
        // --- The symbols "I", "X", "C", and "M" can be repeated three times in succession, but no more
        // --- "I" can be subtracted from "V" and "X" only
        // --- "X" can be subtracted from "L" and "C" only
        // --- "C" can be subtracted from "D" and "M" only
        // --- "V", "L", and "D" can never be subtracted
        // --- Only one small-value symbol may be subtracted from any large-value symbol

        String credits = commandArray[commandArray.length - 1];

        if (credits.equalsIgnoreCase(CREDITS_STRING)) {
            String creditValueInStr = commandArray[commandArray.length - 2];
            String isString = commandArray[commandArray.length - 3];
            String itemName = commandArray[commandArray.length - 4];
            Integer itemIndex = commandArray.length - 4;
            Integer creditValue = 0;
            Integer convertedCurrencyValue = 0;

            // command for converting item value : handling invalid request
            // 1. command format is invalid
            if (!isString.equalsIgnoreCase(IS_STRING))
                System.out.println("[INVALID] I have no idea what you are talking about");

            // 2. credits value not in Integer value
            try {
                creditValue = Integer.parseInt(creditValueInStr);
                System.out.println("[VALID] credit value is written in integer value.");
            } catch (NumberFormatException e) {
                System.out.println("[INVALID] credit value is not written in integer value.");
                valid = false;
            }

            // 3. item value has been converted before
            Integer getItemValue = mapItemToValue.get(itemName);
            if (getItemValue != null) {
                System.out.println("[INVALID] " + itemName + " has been valued before.");
                valid = false;
            } else
                System.out.println("[VALID] " + itemName + " has not been valued before.");

            // 4. currency code is not registered or no currency code has been registered before
            String romanNumeric = "";
            List<String> listCurrency = new ArrayList<String>(mapRomanToCurrency.values());
            if (listCurrency.size() > 0) {
                for (int x = 0; x < itemIndex; x++) {
                    String currencyCode = commandArray[x];
                    if (!listCurrency.contains(currencyCode)) {
                        System.out.println("[INVALID] " + currencyCode + " has not been registered as currency code.");
                        valid = false;
                    } else {
                        romanNumeric = romanNumeric + mapCurrencyToRoman.get(currencyCode);
                        convertedCurrencyValue = convertedCurrencyValue + convertCurrencyCodeIntoInteger(currencyCode, mapCurrencyToRoman);
                        System.out.println("[VALID] convertedCurrencyValue = " + convertedCurrencyValue);
                    }
                }
            } else {
                System.out.println("[INVALID] no currency code has been registered.");
                valid = false;
            }

            // 5. currency code is written in valid roman numeric rule
            if (!validRomanNumeric(romanNumeric)) {
                System.out.println("[INVALID] " + romanNumeric + " is not valid roman numeric.");
                valid = false;
            }

            if (valid) {
                Integer oneItemValue = creditValue / convertedCurrencyValue;
                mapItemToValue.put(itemName, oneItemValue);
                System.out.println("[SUCCESS] one " + itemName + " is equal to " + oneItemValue + " credits.");
            }

        }
        // code blocks for convert item value from credits into currency code - END

        // --------------------------------------------------------------------------------

    }

    public static void printAllRegisteredCurrencyCode(Map<String, String> mapRomanToCurrency, Map<String, String> mapCurrencyToRoman) {
        List<String> listCurrency = new ArrayList<String>(mapRomanToCurrency.values());
        for (int x = 0 ; x < listCurrency.size() ; x++) {
            String currencyCode = listCurrency.get(x);
            String roman = mapCurrencyToRoman.get(currencyCode);
            System.out.println(roman + " has been assigned by " + currencyCode);
        }
    }

    public static Integer convertCurrencyCodeIntoInteger(String currencyCode, Map<String, String> mapCurrencyToRoman) {
        Integer convertedValue = 0;
        String romanNumeric = mapCurrencyToRoman.get(currencyCode);
        switch (romanNumeric) {
            case ONE_IN_ROMAN:
                convertedValue = 1;
                break;
            case FIVE_IN_ROMAN:
                convertedValue = 5;
                break;
            case TEN_IN_ROMAN:
                convertedValue = 10;
                break;
            case FIFTY_IN_ROMAN:
                convertedValue = 50;
                break;
            case ONE_HUNDRED_IN_ROMAN:
                convertedValue = 100;
                break;
            case FIVE_HUNDRED_IN_ROMAN:
                convertedValue = 500;
                break;
            case ONE_THOUSAND_IN_ROMAN:
                convertedValue = 1000;
                break;
        }

        return convertedValue;
    }

    public static boolean validRomanNumeric(String romanNumeric) {
        return romanNumeric.matches(REGEX_ROMAN_NUMERIC);
    }
}
